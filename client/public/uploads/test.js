
var [a,b]=[
    
      {name:"ReactJS" , location:"Belgaum"},
      {name:"VueJS" , location:"Bangalore"},
      {name:"Angular" , location:"Pune"}
    ]
 
console.log('\n\n\n', a ,b,'\n\n\n');

var b=()=>{
    a=10;
  //  console.log(a);
}
b();


console.log(a);

var z=a=> a*a;
console.log('----Z------',z(10), '\n');

var arrow = (name)=> name.split(' ')[0];
 

console.log(arrow('Manjunath Jadhav'));

var user={
    name:"Manjunath",
    cities:['Bgm', 'Blore','Pune'],

    printPlacesLived:function(city){
        this.cities.forEach((city)=>{
            console.log(this.name +' has lived in '+city);
        });
    }
};
user.printPlacesLived();
console.log('-------------------------------------\n');

var multiplier = {
    numbers:[2,3,4],
    multiplyBy:2,

    calculation(){
        return this.numbers.map((element)=>{
            return this.multiplyBy*element;
        });
    }
};

console.log(multiplier.calculation());
console.log('-------------------------------------\n');
var multiplier = {
    numbers:[2,3,4],
    multiplyBy:3,

    calculation(){
        return this.numbers.map((element)=> this.multiplyBy*element );
    }
};
console.log(multiplier.calculation());

//....................
//let vs var

//var exapmle
var a;
if(10>0){
    a=10
    console.log(a);
}
console.log(a, 'var out of block ',a+a);
{
  console.log(c); // undefined. Due to hoisting
  var c = 2;
}


//let example
let letB;
if(10>0){
    letB=10;
    console.log(letB, 'LetB inside the block');
}
console.log(letB, 'LetB outside the block');


for( let j = 0; j < 3; j++ ) {
    console.log(j);
  };

//console.log(j, 'error cannot use let outside the block'); // 3

{
  console.log(b); // ReferenceError: b is not defined
  let b = 3;
}
