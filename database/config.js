const mongoose=require('mongoose');

// warning
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

var url = 'mongodb://localhost:27017/nodeblog';
// var url = 'mongodb://localhost:27017/bysMean';
mongoose.connect(url,{ useNewUrlParser: true }, (error)=>{
    if(!error){
        console.log(' Db connected!!! \n');
    }
});
module.exports=mongoose;